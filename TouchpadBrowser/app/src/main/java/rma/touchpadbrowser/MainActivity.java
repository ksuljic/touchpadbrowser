package rma.touchpadbrowser;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.HapticFeedbackConstants;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity implements View.OnTouchListener {

    private WebView myWebView; // main webview used to show web pages
    private ProgressBar progressBar; // progress bar showing progress of page loading
    private EditText urlTextBox; // edit text field
    private String url; // edit text content
    private ImageView activityButton;
    private Context context;
    private String value;

    public final static String EXTRA_MESSAGE = "rma.touchpadbrowser.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();

        myWebView = (WebView) findViewById(R.id.webview); // setting webview from activity_main.xml to myWebView variable
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setBuiltInZoomControls(true); // enabling zoom
        myWebView.loadUrl("https://www.google.com"); // default url

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setMax(100); //setting progress bar max value to 100

        urlTextBox = (EditText) findViewById(R.id.editText);
        urlTextBox.setFocusableInTouchMode(false); //by default edit text cannot be focusable
        urlTextBox.setFocusable(false);
        urlTextBox.setSelectAllOnFocus(true);// when focused highlights (selects) all content

        activityButton = (ImageView) findViewById(R.id.floatingButton);
        activityButton.setOnTouchListener(this);

        myWebView.setWebViewClient(new WebViewClient() {
            // setting new webviewclient in order to define what happens
            // after certain events

            @Override
            public void onPageFinished(WebView view, String url) {
                urlTextBox.setText(url);
                urlTextBox.setFocusableInTouchMode(true);
                urlTextBox.setFocusable(true);

            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);

            }
        });

        myWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress < 100 && progressBar.getVisibility() == progressBar.GONE) {
                    progressBar.setVisibility(progressBar.VISIBLE);
                }
                progressBar.setProgress(newProgress);

                if (newProgress == 100) {
                    progressBar.setVisibility(progressBar.GONE);
                }
                super.onProgressChanged(view, newProgress);
            }
        });

        urlTextBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                url = "https://www.google.com/search?q=" + v.getText();
                myWebView.loadUrl(url);
                return false;
            }
        });

        urlTextBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        final Button refreshButton = (Button) findViewById(R.id.button);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myWebView.reload();
                urlTextBox.setFocusable(false);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && myWebView.canGoBack()) {
            myWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.action_settings) {

            AlertDialog.Builder alert = new AlertDialog.Builder(this);

            alert.setTitle("Settings");
            alert.setMessage("Enter IP adress of the remote PC:");

// Set an EditText view to get user input
            final EditText input = new EditText(this);
            alert.setView(input);

            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    value = input.getText().toString();
                    // Do something with value!
                }
            });

            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Canceled.
                }
            });

            alert.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        //handle 'activity button' click and create a new intent
        //forward current url to the new intent
        Intent intent = new Intent(MainActivity.this, TouchpadActivity.class);
        String string = urlTextBox.getText().toString();
        String val = value;
        intent.putExtra(EXTRA_MESSAGE, string);
        intent.putExtra("val", val);
        startActivity(intent);
        activityButton.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
        return false;
    }

}
