package rma.touchpadbrowser;

public class Constants {
    public static final int SERVER_PORT = 8998;
    public static final String SCROLLUP="scrollup";
    public static final String SCROLLDOWN="scrolldown";
    public static final String MOUSE_LEFT_CLICK="left_click";
    public static final String MOUSE_RIGHT_CLICK ="right_click";
    public static final String CLOSE_TAB ="closetab";
    public static final String COPY = "copy";
    public static final String PASTE = "paste";
    public static final String NEW_TAB = "new_tab";

}
