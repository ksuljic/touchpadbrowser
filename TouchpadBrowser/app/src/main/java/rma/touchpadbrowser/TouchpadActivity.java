package rma.touchpadbrowser;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;


public class TouchpadActivity extends ActionBarActivity implements View.OnClickListener {

    private Context context;
    private Button KeyboardButton, CopyButton, PasteButton ;
    private TextView mousePad;
    private TextView rightClick;
    private TextView leftClick;
    private InputMethodManager imn;
    private InetAddress clientAddr;

    private boolean isConnected=false;
    private Socket socket;
    private PrintWriter out;

    private float initX = 0;
    private float initY = 0;
    private float disX = 0;
    private float disY = 0;

    private String urlToSend;
    private String serverIP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touchpad);
        Intent intent = getIntent();
        //get the URL sent from previous intent and store it to urlToSend
        urlToSend = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        serverIP = intent.getStringExtra("val");
        Log.d("ip", String.valueOf(serverIP));
        Log.d("ip", String.valueOf(urlToSend));

        ConnectPhoneTask connectPhoneTask = new ConnectPhoneTask();
        connectPhoneTask.execute(serverIP); //try to connect to server in another thread

        context = this; //save the context to show Toast messages

        //Get references of all buttons
        KeyboardButton = (Button)findViewById(R.id.KeyButton);
        CopyButton = (Button)findViewById(R.id.CopyButton);
        PasteButton = (Button)findViewById(R.id.PasteButton);

        //this activity extends View.OnClickListener, set this as onClickListener
        //for all buttons
        KeyboardButton.setOnClickListener(this);
        CopyButton.setOnClickListener(this);
        PasteButton.setOnClickListener(this);

        //Get reference to the TextView acting as mousepad
        //same for right and left click
        mousePad = (TextView)findViewById(R.id.mousePad);
        rightClick = (TextView) findViewById(R.id.rightClick);
        leftClick = (TextView) findViewById(R.id.leftClick);

        handleRightClick(rightClick);
        handleLeftClick(leftClick);

        imn = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        //capture finger taps and movement on the textview
        mousePad.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int touchCounter = event.getPointerCount();
                if (isConnected && out != null && touchCounter == 1) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            //save X and Y positions when user touches the TextView
                            initX = event.getX();
                            initY = event.getY();
                            break;
                        case MotionEvent.ACTION_MOVE:
                            disX = event.getX() - initX; //Mouse movement in x direction
                            disY = event.getY() - initY; //Mouse movement in y direction
                            /*set init to new position so that continuous mouse movement
                            is captured*/
                            initX = event.getX();
                            initY = event.getY();
                            if (disX != 0 || disY != 0) {
                                out.println(disX + "," + disY); //send mouse movement to server
                            }
                            break;
                        case MotionEvent.ACTION_UP:
                            long deltaTime = event.getEventTime() - event.getDownTime();
                            if (deltaTime < 250) {
                                //if event time is less than 250 ms trigger left click
                                out.println(Constants.MOUSE_LEFT_CLICK);
                                mousePad.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                            }
                            break;
                    }
                }else if(touchCounter == 2){
                    //if two fingers are on mouse pad
                    if(event.getAction() == MotionEvent.ACTION_MOVE){
                        float deltaY = event.getY() - initY;
                        float tolerance = 10;

                        if (deltaY > tolerance){
                            out.println(Constants.SCROLLUP);
                            initY = event.getY();
                        }
                        else if(deltaY < -1 * tolerance){
                            out.println(Constants.SCROLLDOWN);
                            initY = event.getY();
                        }
                    }else initY = event.getY();

                }
                return true;
            }
        });
    }

    private void handleLeftClick(final TextView leftClick) {
        leftClick.setOnTouchListener(new GestureListener(this) {
            @Override
            public void onSingleTapUp() {
                out.println(Constants.MOUSE_LEFT_CLICK);
                leftClick.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
            }

            @Override
            public void onLongPress() {
                out.println(Constants.NEW_TAB);
                leftClick.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
            }

            @Override
            public void onSwipeRight() {
                out.println(urlToSend);
            }
        });
    }

    private void handleRightClick(final TextView rightClick) {
        rightClick.setOnTouchListener(new GestureListener(this) {
            @Override
            public void onSingleTapUp() {
                out.println(Constants.MOUSE_RIGHT_CLICK);
                rightClick.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);

            }

            @Override
            public void onSwipeLeft() {
                out.println(Constants.CLOSE_TAB);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        //handle keypresses

        int keyAction = event.getAction();

        if(keyAction == KeyEvent.ACTION_DOWN) {
            int keyCode = event.getUnicodeChar();
            String letter = String.valueOf(keyCode);
            Log.d("code", letter);
            out.println(letter);
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }

    //OnClick method is called when any of the buttons are pressed
    @Override
    public void onClick(View v) {

        switch(v.getId()){

            case R.id.KeyButton:
                imn.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                KeyboardButton.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                break;
            case R.id.CopyButton:
                out.println(Constants.COPY);
                CopyButton.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                break;
            case R.id.PasteButton:
                out.println(Constants.PASTE);
                PasteButton.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                break;

        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(isConnected && out!=null) {
            try {
                out.println("exit"); //tell server to exit
                socket.close(); //close socket

            } catch (IOException e) {
                Log.e("remotedroid", "Error in closing socket", e);
            }
        }
    }

    public class ConnectPhoneTask extends AsyncTask<String,Void,Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            boolean result = true;
            try {
                clientAddr = InetAddress.getByName(params[0]);
                socket = new Socket(clientAddr, Constants.SERVER_PORT);//Open socket on server IP and port
            } catch (IOException e) {
                Log.e("remotedroid", "Error while connecting", e);
                result = false;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            isConnected = result;
            Toast.makeText(context,isConnected?"Connected to server!":"Error while connecting",Toast.LENGTH_LONG).show();
            try {
                if(isConnected) {
                    out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket
                            .getOutputStream())), true); //create output stream to send data to server
                }
            }catch (IOException e){
                Log.e("remotedroid", "Error while creating OutWriter", e);
                Toast.makeText(context,"Error while connecting",Toast.LENGTH_LONG).show();
            }
        }
    }
}